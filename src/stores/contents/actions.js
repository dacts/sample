import {createAsyncThunk} from '@reduxjs/toolkit';
import {getMessages} from '../../shared/services/adapter';

export const fetchData = createAsyncThunk(
  'content/fetchData',
  async (_, {getState, rejectWithValue}) => {
    try {
      const pageToken = getState().content.pageToken;
      //   console.log(pageToken);
      const response = await getMessages(pageToken);
      //   console.log(response.messages);
      return response; //JSON.parse();
    } catch (e) {
      return rejectWithValue(e);
    }
  },
);

export const filterProfile = createAsyncThunk(
  'content/filterProfiles',
  async (value, {getState}) => {
    const data = getState().content.data;
    if (!value) {
      return undefined;
    }
    const filtered = data.filter(e => {
      return e && e.author && e.author.name.startsWith(value, 0);
    });
    return filtered;
  },
);

export const deleteAuthor = createAsyncThunk(
  'content/deleteAuthor',
  async (value, {getState}) => {
    const data = getState().content.data;
    if (data[value - 1].id === data) {
      data.splice(value - 1, 1);
      return data;
    }
    const filtered = data.filter(e => {
      return e.id !== value;
    });
    return filtered;
  },
);
