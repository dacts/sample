import {createSlice} from '@reduxjs/toolkit';
import {fetchData, filterProfile, deleteAuthor} from './actions';

const initialState = {
  data: [],
  filtered: undefined,
  loading: false,
  error: '',
  pageToken: undefined,
};

const ContentReducer = createSlice({
  name: 'content',
  initialState: {...initialState},
  reducers: {},
  extraReducers: {
    [deleteAuthor.fulfilled]: (state, actions) => {
      state.data = actions.payload;
      return state;
    },
    [filterProfile.fulfilled]: (state, actions) => {
      state.filtered = actions.payload;
      return state;
    },
    [fetchData.fulfilled]: (state, actions) => {
      state.data = [...state.data, ...actions.payload.messages];
      state.pageToken = actions.payload.pageToken;
      state.loading = false;
      state.error = '';
      return state;
    },
    [fetchData.rejected]: (state, actions) => {
      state.loading = false;
      state.error = 'Unable to fetch data try again.';
      return state;
    },
    [fetchData.pending]: (state, actions) => {
      state.loading = true;
      return state;
    },
  },
});

export default ContentReducer.reducer;
