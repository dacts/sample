import {createSlice} from '@reduxjs/toolkit';
import {fetchData, filterProfile} from './actions';

const initialState = {
  likes: {},
};

const MetaReducer = createSlice({
  name: 'meta',
  initialState: {...initialState},
  reducers: {
    setLike: (state, actions) => {
      if (state.likes[actions.payload]) {
        delete state.likes[actions.payload];
        return state;
      }
      state.likes[actions.payload] = true;
      return state;
    },
  },
  extraReducers: {},
});

export const {setLike} = MetaReducer.actions;

export default MetaReducer.reducer;
