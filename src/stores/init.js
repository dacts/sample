import {configureStore} from '@reduxjs/toolkit';

import contentReducer from './contents/reducer';
import metaReducer from './meta/reducer';

const store = configureStore({
  reducer: {
    content: contentReducer,
    meta: metaReducer,
  },
});

export default store;
