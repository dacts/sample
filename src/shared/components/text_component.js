import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {useTheme, makeStyles} from '@rneui/themed';

export const TextComponent = ({children, size, color, weight, center}) => {
  const styles = useStyles();
  return (
    <Text
      style={[
        size ? styles[size] : styles.default,
        weight ? styles[weight] : styles.default,
        color ? styles[color] : {color: '#333'},
        center ? styles.center : styles.default,
      ]}>
      {children}
    </Text>
  );
};

const useStyles = makeStyles(theme => {
  // console.log(theme);
  return {
    center: {textAlign: 'center'},
    light: {fontFamily: 'IBMPlexSansArabic-Light'},
    normal: {fontFamily: 'IBMPlexSansArabic-Regular'},
    semiBold: {fontFamily: 'IBMPlexSansArabic-Medium'},
    bold: {fontFamily: 'IBMPlexSansArabic-SemiBold'},
    extraBold: {fontFamily: 'IBMPlexSansArabic-Bold'},
    xs: {fontSize: 12},
    md: {fontSize: 14},
    lg: {fontSize: 16},
    xl: {fontSize: 18},
    xxl: {fontSize: 22},
    primary: {color: theme.colors.primary},
    secondary: {color: theme.colors.secondary},
    danger: {color: theme.colors.danger},
    text: {color: theme.colors.text},
    default: {},
  };
});
