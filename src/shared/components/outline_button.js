import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {useTheme} from '@rneui/themed';

import {TextComponent} from './text_component';
// Styles
import cs from '../../styles/common_styles';

export const OutlineButton = props => {
  const {colors} = useTheme().theme;
  const border = {borderColor: colors.danger, borderRadius: 20};
  return (
    <TouchableOpacity onPress={props.onClick}>
      <View style={[cs.h35, cs.jc, cs.ac, cs.ph16, cs.bw2, border]}>
        <TextComponent weight="bold" color="danger" size="xs">
          Delete
        </TextComponent>
      </View>
    </TouchableOpacity>
  );
};
