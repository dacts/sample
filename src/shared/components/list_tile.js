import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Avatar} from '@rneui/base';

import {TextComponent} from './text_component';
// Styles
import cs from '../../styles/common_styles';

export const ListTileComponent = ({imageUrl, title, subtitle, onClick}) => {
  return (
    <TouchableOpacity activeOpacity={0.95} onPress={onClick}>
      <View style={[cs.row, cs.js, cs.ac]}>
        <Avatar size={50} rounded source={{uri: imageUrl}} />
        <View style={[cs.ph8, cs.w155]}>
          <TextComponent color="primary" size="md" weight="bold">
            {title}
          </TextComponent>
          <TextComponent color="primary" size="xs" weight="normal">
            {subtitle}
          </TextComponent>
        </View>
      </View>
    </TouchableOpacity>
  );
};
