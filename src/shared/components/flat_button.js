import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {useTheme} from '@rneui/themed';

import {TextComponent} from './text_component';
// Styles
import cs from '../../styles/common_styles';

export const FlatButton = props => {
  const {colors} = useTheme().theme;
  const border = {borderColor: colors.danger, borderRadius: 20};
  return (
    <TouchableOpacity onPress={props.onClick}>
      <View style={[cs.h35, cs.jc, cs.ac, cs.ph16]}>
        <TextComponent weight="bold" color="black" size="lg">
          Cancel
        </TextComponent>
      </View>
    </TouchableOpacity>
  );
};
