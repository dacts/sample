import React from 'react';
import {View, StatusBar} from 'react-native';

// styles
import styles from '../../styles/common_styles';

export const LayoutComponent = ({children, center}) => {
  const background = {backgroundColor: '#fff'};
  return (
    <>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <View
        style={[
          styles.w100p,
          styles.flex1,
          styles.js,
          center ? styles.ac : styles.as,
          styles.ph8,
          background,
        ]}>
        {children}
      </View>
    </>
  );
};
