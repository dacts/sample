import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';

// Styles
import cs from '../../styles/common_styles';

export const InputField = ({placeHolder, value, onChange}) => {
  return (
    <TextInput
      onChangeText={onChange}
      style={[styles.input]}
      placeholder={placeHolder}
      value={value}
      placeholderTextColor="rgba(46,52,66,1.0)"
    />
  );
};

const styles = new StyleSheet.create({
  input: {
    borderWidth: 0,
    width: '100%',
    color: '#333', // 'rgba(30,37,50,1.0)',
    fontWeight: 'bold',
  },
});
