import axios from 'axios';
import {endPoint} from './endpoints';

const instance = axios.create({baseURL: endPoint.BASE_URL});

export async function getMessages(token) {
  const params = {pageToken: token};
  return (await instance.get(endPoint.MESSAGE, {params: params})).data;
}
