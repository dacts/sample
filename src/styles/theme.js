import {createTheme} from '@rneui/themed';
const theme = {
  lightColors: {
    primary: 'rgba(46,52,66,1.0)',
    secondary: 'rgba(244, 246, 249, 1.0)',
    danger: 'rgba(255, 121, 114, 1.0)',
    text: '#333',
  },

  Text: {},
};

export default theme;
