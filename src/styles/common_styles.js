import {StyleSheet} from 'react-native';

const styles = new StyleSheet.create({
  flex1: {flex: 1},
  /* Position  */

  row: {flexDirection: 'row'},

  js: {justifyContent: 'flex-start'},
  jb: {justifyContent: 'space-between'},
  jc: {justifyContent: 'center'},
  je: {justifyContent: 'flex-end'},

  as: {alignItems: 'flex-start'},
  ac: {alignItems: 'center'},

  /* Width  */

  // Percentage
  w100p: {width: '100%'},

  // Fixed
  w8: {width: 8},
  w16: {width: 16},
  w30: {width: 30},
  w55: {width: 55},
  w155: {width: 155},

  /* Height  */
  h100p: {height: '100%'},
  // Fixed
  h50: {height: 50},
  h40: {height: 40},
  h30: {height: 30},
  h35: {height: 35},
  h80: {height: 80},

  /* Border  */
  br25: {borderRadius: 25},
  br16: {borderRadius: 16},

  bw2: {borderWidth: 2},
  bw1: {borderWidth: 1},
  /* Padding  */
  p24: {padding: 24},
  // Horizontal
  ph16: {paddingHorizontal: 16},
  ph8: {paddingHorizontal: 8},
  // Vertical
  pv8: {paddingVertical: 8},
  pv16: {paddingVertical: 16},
  /* Margin  */

  mt16: {marginTop: 16},

  // colors
  modalBg: {backgroundColor: 'rgba(0,0,0,0.2)'},
});

export default styles;
