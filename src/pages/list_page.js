import React from 'react';
import {View, FlatList} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useAsyncFn} from 'react-use';
// Components
import {LayoutComponent} from '../shared/components/component';
import Search from './component/search';
import UserCard from './component/user_card';
import LoadMore from './component/load_more';

import {fetchData} from '../stores/contents/actions';

const ListPages = ({navigation}) => {
  const data = useSelector(state => state.content && state.content.data);
  const filtered = useSelector(state => state.content.filtered);
  // console.log(data);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(fetchData());
  }, [dispatch]);

  const showDetails = details => {
    navigation.navigate('DetailPage', {...details});
  };

  return (
    <LayoutComponent>
      <Search />
      <View style={[{width: '100%', flex: 1}]}>
        <FlatList
          data={filtered || data}
          keyExtractor={item => item.id}
          renderItem={({item, index}) => {
            return (
              <UserCard onClick={showDetails} data={item} position={index} />
            );
          }}
          ListFooterComponent={<LoadMore />}
        />
      </View>
    </LayoutComponent>
  );
};

export default ListPages;
