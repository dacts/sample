import React from 'react';
import {View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useTheme} from '@rneui/themed';
import {Icon} from '@rneui/base';

import {TextComponent} from '../../shared/components/component';

// styles
import cs from '../../styles/common_styles';
//filterProfile
import {setLike} from '../../stores/meta/reducer';

const Header = ({id, goBack}) => {
  const {colors} = useTheme().theme;
  const dispatch = useDispatch();
  const isLiked = useSelector(state => state.meta.likes[id]);
  return (
    <View style={[cs.row, cs.jb, cs.ac, cs.h50, cs.w100p, cs.ph8]}>
      <View style={[cs.row, cs.js, cs.ac]}>
        <Icon
          onPress={goBack}
          type="font-awesome"
          name="chevron-left"
          color={colors.danger}
        />
        <View style={[cs.ph8]} />
        <TextComponent size="xxl" weight="bold">
          Details
        </TextComponent>
      </View>
      <Icon
        size={20}
        underlayColor="white"
        onPress={() => dispatch(setLike(id))}
        color={isLiked ? 'red' : 'rgba(134,142,151,1.0)'}
        type="font-awesome"
        name={isLiked ? 'heart' : 'heart-o'}
      />
    </View>
  );
};

export default Header;
