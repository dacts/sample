import React from 'react';
import {View} from 'react-native';
import {useDispatch} from 'react-redux';
import {makeStyles} from '@rneui/themed';
import {Icon} from '@rneui/base';
// Components
import {InputField} from '../../shared/components/component';
// styles
import cs from '../../styles/common_styles';
//filterProfile
import {filterProfile} from '../../stores/contents/actions';

let timer;
const Search = () => {
  const styles = useStyls();
  const dispatch = useDispatch();
  const [value, setValue] = React.useState('');
  const onChange = v => {
    try {
      clearTimeout(timer);
    } catch (e) {}
    timer = setTimeout(() => {
      dispatch(filterProfile(v));
    }, 1500);
    setValue(v);
  };
  return (
    <View
      style={[
        styles.background,
        cs.row,
        cs.jc,
        cs.ac,
        cs.ph16,
        cs.mt16,
        cs.h50,
        cs.br25,
      ]}>
      <View style={[cs.flex1]}>
        <InputField value={value} onChange={onChange} placeHolder="Search..." />
      </View>
      <View style={[cs.w30]}>
        <Icon name="search" color="rgba(134,142,151,1.0)" />
      </View>
    </View>
  );
};

export default Search;

const useStyls = makeStyles(theme => ({
  background: {backgroundColor: theme.colors.secondary},
}));
