import React from 'react';
import {View, Modal} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {Icon} from '@rneui/base';
import {useTheme} from '@rneui/themed';
import dayjs from 'dayjs';

import {
  ListTileComponent,
  OutlineButton,
  TextComponent,
  FlatButton,
} from '../../shared/components/component';

// Utils
import {endPoint} from '../../shared/services/endpoints';
// styles
import cs from '../../styles/common_styles';
import {setLike} from '../../stores/meta/reducer';
import {deleteAuthor} from '../../stores/contents/actions';

const data = {
  content:
    'Well, any friend of Bingley, and a variety of all that his manners are very cautious, I suppose, as to be sincere did you say that a connection with her, that these sort of extraordinary advantage and blessing which few can boast.',
  updated: '2015-02-01T12:41:23Z',
  id: 11,
  author: {name: 'William Shakespeare', photoUrl: '/photos/jane-austen.jpg'},
};
const UserCard = props => {
  const dispatch = useDispatch();
  const {colors} = useTheme().theme;
  const borderColor = {borderColor: colors.secondary};
  const imageUrl = endPoint.BASE_URL + props.data.author.photoUrl;
  const years = `${dayjs().diff(props.data.updated, 'years')} years ago`;
  const isLiked = useSelector(state => state.meta.likes[props.data.id]);
  const [visible, setVisible] = React.useState(false);
  const onSelect = () => {
    props.onClick(props.data);
  };

  const onDelete = () => {
    setVisible(v => !v);
  };

  const confirmDelete = () => {
    dispatch(deleteAuthor(props.data.id));
  };

  return (
    <>
      <View
        style={[
          cs.w100p,
          cs.row,
          cs.jb,
          cs.ac,
          cs.bw2,
          cs.ph8,
          cs.pv16,
          cs.br16,
          cs.mt16,
          borderColor,
        ]}>
        <ListTileComponent
          onClick={onSelect}
          imageUrl={imageUrl}
          title={props.data.author.name}
          subtitle={years}
        />
        <View style={[cs.row, cs.jc, cs.ac, cs.flex1]}>
          <Icon
            size={20}
            underlayColor="white"
            onPress={() => dispatch(setLike(props.data.id))}
            color={isLiked ? 'red' : 'rgba(134,142,151,1.0)'}
            type="font-awesome"
            name={isLiked ? 'heart' : 'heart-o'}
          />
          <View style={[cs.w16]} />
          <OutlineButton onClick={onDelete} />
        </View>
      </View>
      <Modal
        visible={visible}
        transparent={true}
        onRequestClose={() => setVisible(v => !v)}>
        <View
          style={[
            cs.w100p,
            cs.h100p,
            cs.modalBg,
            cs.flex1,
            cs.ph16,
            cs.jc,
            cs.ac,
          ]}>
          <View
            style={[
              cs.w100p,
              {height: 250, backgroundColor: 'white', borderRadius: 16},
              cs.p24,
            ]}>
            <TextComponent size="xxl" weight="extraBold" color="text">
              Delete this author?
            </TextComponent>
            <View style={[cs.pv8]} />
            <ListTileComponent
              imageUrl={imageUrl}
              title={props.data.author.name}
              subtitle={years}
            />
            <View style={[cs.row, cs.je, cs.ac, cs.flex1]}>
              <FlatButton onClick={() => setVisible(v => !v)} />
              <View style={[cs.w16]} />
              <OutlineButton onClick={confirmDelete} />
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default UserCard;
