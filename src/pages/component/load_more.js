import React from 'react';
import {View, TouchableOpacity, ActivityIndicator} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useTheme} from '@rneui/themed';

// Components
import {TextComponent} from '../../shared/components/component';
// styles
import cs from '../../styles/common_styles';

import {fetchData} from '../../stores/contents/actions';

const LoadMore = () => {
  const dispatch = useDispatch();
  const {colors} = useTheme().theme;
  const data = useSelector(state => state.content && state.content.filtered);
  const token = useSelector(state => state.content && state.content.pageToken);
  const loading = useSelector(state => state.content && state.content.loading);
  const onClickLoad = () => {
    dispatch(fetchData());
  };
  return data || !token || loading ? (
    <View style={[cs.jc, cs.ac, cs.ph16, loading && cs.h80, cs.br25]}>
      {loading && <ActivityIndicator size={20} color={colors.danger} />}
    </View>
  ) : (
    <TouchableOpacity onPress={onClickLoad}>
      <View style={[cs.jc, cs.ac, cs.ph16, cs.h80, cs.br25]}>
        <TextComponent size="lg" weight="bold" color="danger">
          Load More
        </TextComponent>
      </View>
    </TouchableOpacity>
  );
};

export default LoadMore;
