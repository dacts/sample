import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Screens
import ListPage from './list_page';
import DetailPage from './detail';

const Stack = createNativeStackNavigator();

const Pages = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen options={{headerShown: false}} name="Home">
          {props => <ListPage {...props} />}
        </Stack.Screen>
        <Stack.Screen options={{headerShown: false}} name="DetailPage">
          {props => <DetailPage {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Pages;
