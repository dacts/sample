import React from 'react';
import {View} from 'react-native';
import {Avatar} from '@rneui/base';
// Components
import {LayoutComponent, TextComponent} from '../shared/components/component';
import Header from './component/header';
import {endPoint} from '../shared/services/endpoints';

import cs from '../styles/common_styles';

const DetailPages = props => {
  const imageUrl = endPoint.BASE_URL + props.route.params.author.photoUrl;
  return (
    <LayoutComponent center>
      <Header
        id={props.route.params.id}
        goBack={() => props.navigation.goBack()}
      />
      <View style={[cs.pv8]} />
      <Avatar size={250} rounded source={{uri: imageUrl}} />
      <View style={[cs.pv8]} />
      <TextComponent weight="bold" size="xxl" color="text">
        {props.route.params.author.name}
      </TextComponent>
      <View style={[cs.pv8]} />
      <TextComponent weight="bold" size="xl" color="text" center>
        {props.route.params.content}
      </TextComponent>
    </LayoutComponent>
  );
};

export default DetailPages;
