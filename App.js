import React from 'react';
import {Provider} from 'react-redux';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import {ThemeProvider, Button, createTheme} from '@rneui/themed';

import Pages from './src/pages/pages';
import themeLight from './src/styles/theme';

import store from './src/stores/init';

const App = () => {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <ThemeProvider theme={themeLight}>
          <Pages />
        </ThemeProvider>
      </Provider>
    </SafeAreaProvider>
  );
};

export default App;
